<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="submit" form="form-exelgen" data-toggle="tooltip" title="<?php echo $button_save; ?>"
					class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>"
					class="btn btn-default"><i class="fa fa-reply"></i></a></div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-exelgen"class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-2 control-label"
							for="input_exelgen_status"><?php echo $entry_status; ?></label>
						<div class="col-sm-10">
							<select name="exelgen_status" id="input_exelgen_status" class="form-control">
								<?php if ($exelgen_status) { ?>
								<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
								<option value="0"><?php echo $text_disabled; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_enabled; ?></option>
								<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<?php if ($entry_status): ?>
					<div class="form-group">
						<div class="col-sm-2">
							<div class="control-label">Выберите тип сортировки:</div>
						</div>
						<div class="col-sm-10">
							<label class="label_radio_buttons_type_sort"><input name="type_sort" type="radio" value="of_types" checked>Формировать лист на каждый тип товара</label>
							<label class="label_radio_buttons_type_sort"><input name="type_sort" type="radio" value="of_count">Формировать лист на каждую 1000 товаров</label>
							<label class="label_radio_buttons_type_sort"><input name="type_sort" type="radio" value="of_all">Формировать один лист для всех товаров</label>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-2">
							<div class="control-label">Выберите опции выборки товаров:</div>
						</div>
						<div class="col-sm-2">
							
							<label class="label_checkbox_buttons_type_sort"><input name="is_active_product" type="checkbox" checked>Только активные товары</label>
							<label class="label_checkbox_buttons_type_sort"><input name="is_active_category" type="checkbox" checked>Только активные категории</label>
						</div>
						<div class="col-sm-8">
							<p style="color: red;">&#9940; Внимание! &#9940;<br />Использовать эти настройки нужно только если вы точно понимаете что делаете, неадекватное использование может привести к поразительным результатам.</p>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-2"></div>
						<div class="col-sm-10">
							<button id="GenerationExelDocument" class="btn btn-primary"><?=$button_generate?></button>
							<!-- <button id="RenameDefaultImage" class="btn btn-primary btn-green">Переименовать картинки товаров</button> -->
							<!-- <a id="DownloadExelDocument" href="/system/storage/download/text.csv" class="btn btn-primary btn-green"><?=$button_download?></a> -->
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-2">
							<div class="control-label">
								<?=$text_title_files_list?>
							</div>
						</div>
						<div class="col-sm-10">
							<ol class="files_list">
								<?php if (!empty($files)): ?>
									<?php foreach($files as $file): ?>
									<li class="files_list__item">
										<a target="_blank" href="/exelgen/<?=$file?>"><?=$file?></a><button class="delete_file" data-file="<?=$file?>" title="Удалить файл">&#9940;</button>
									</li>
									<?php endforeach ?>
								<?php else: ?>
									<span>Ранее созданных файлов не обнаружено</span>
								<?php endif ?>

							</ol>
						</div>
					</div>
					<div id="displayStatus" class="form-group" style="display: none;">
						<div class="col-sm-2">
						</div>
						<div class="col-sm-10">
						
						</div>
					</div>
					<pre id="displayError" style="display: none;">
						<!-- Plache For Errors -->
					</pre>
					<?php endif ?>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="col-12">
</div>
<style>
	.form-group label{
		margin-bottom: 0;
	}
	.btn-green{
		background: green;
	}
	.files_list{
		column-count: 3;
		padding-left: 15px;
	}
	.files_list__item{
		margin-bottom: 10px;
	}
	.label_radio_buttons_type_sort{
		display: flex;
		align-items: center;
	}
	.label_radio_buttons_type_sort input[type="radio"]{
		margin-top: -3px;
		margin-right: 5px;
	}
	.label_checkbox_buttons_type_sort{
		display: flex;
	}
	.label_checkbox_buttons_type_sort input[type="checkbox"]{
		margin-right: 5px;
	}
	.delete_file{
		background: transparent;
		border: none;
	}
</style>


<script>
$('#GenerationExelDocument').on('click', function(e) {
	console.log('Генерировать документ!');
	$(this).prop('disabled', true);
	$('#RenameDefaultImage').prop('disabled', true);
	let type_sort = $('input[name=type_sort]:checked').val();
	let filter_active_product = $('input[name=is_active_product]').is(":checked");
	let filter_active_category = $('input[name=is_active_category]').is(":checked");
	// console.log(filter_active_category);
	e.preventDefault();
	$('#displayError').hide();
	$('#displayError').empty();
	$('#displayStatus').hide();
	$('#displayStatus div').empty();
	$.ajax({
		url:'index.php?route=extension/module/exelgen/exelgen&token=<?=$token?>',
		dataType: 'json',
		type: 'POST',
		data: 'type_sort='+type_sort+'&filter_active_product='+filter_active_product+'&filter_active_category='+filter_active_category,
		beforeSend: function(){
			$('#displayStatus').show();
			$('#displayStatus div.col-sm-10').append('<p>Формирование документа. Инициировано... Ожидайте.<br />Среднее время ожидания ответа от 10 до 40 секунд...</p>');
		},
		complete: function(){
			$('#displayStatus div.col-sm-10').append('<p>Формирование документа. Запрос обработан.</p>');
		},
		success: function(json){
			console.log(json);
			$('#displayStatus div.col-sm-10').append('<p>Формирование документа. Ответ получен...</p>');
			$('#displayStatus div.col-sm-10').append('<p>Формирование документа. Затрачено время на обработку: ' + json['delta'] + '</p>');
			$('#displayStatus div.col-sm-10').append('<p>Формирование документа. Колличество иттераций: ' + json['product_count'] + '</p>');
			$('#displayStatus div.col-sm-10').append('<p>Формирование документа. Колличество элементов в документе: ' + json['elements'] + '</p>');
			$('#displayStatus div.col-sm-10').append('<p>Документ доступен для скачивания. <a target="_blank" href="' + json['document_link'] + '">' + json['document_link'] + '</a></p>');
			$('#GenerationExelDocument').prop('disabled', false);
			$('#RenameDefaultImage').prop('disabled', false);
		},
		error: function(jqXHR, textStatus, errorThrown){
			$('#displayError').show();
			$('#displayError').append('server response: ' + textStatus + '\n\r');
			$('#displayError').append('error description: ' + errorThrown);
			$('#displayError').append('<span style="color: red;">   Файл не создан!</span>');
			console.log('response: ' + textStatus); // пoкaжeм oтвeт сeрвeрa
			console.log('error description: ' + errorThrown); // и тeкст oшибки
		}
	});
});
$('.delete_file').on('click', function(e){
	e.preventDefault();
	let file = $(this).attr('data-file');
	$(this).parent('.files_list__item').remove();
	$('#displayError').hide();
	$('#displayError').empty();
	$('#displayStatus').hide();
	$('#displayStatus div.col-sm-10').empty();
	console.log('Удален файл: ' + file);
	$.ajax({
		url:'index.php?route=extension/module/exelgen/delete&token=<?=$token?>',
		dataType: 'json',
		type: 'POST',
		data: 'file='+file,
		beforeSend: function(){
			$('#displayStatus').show();
			$('#displayStatus div.col-sm-10').append('<p>Инициализировано удаление файла: '+file+'</p>');
		},
		success: function(json){
			$('#displayStatus div.col-sm-10').append('<p style="color: red;">Файл был успешно удален</p>');
		}
	});
});
$('#RenameDefaultImage').on('click', function(e){
	e.preventDefault();
	$(this).prop('disabled', true);
	$('#GenerationExelDocument').prop('disabled', true);
	console.log('Переименовываем файлы, Клавиша заблокирована');
	$.ajax({
		url:'index.php?route=extension/module/exelgen/renameFiles&token=<?=$token?>',
		dataType: 'json',
		type: 'POST',
		success: function(json){
			console.log(json);
			$('#GenerationExelDocument').prop('disabled', false);
			$('#RenameDefaultImage').prop('disabled', false);
		},
		error: function(xhr, ajaxOpions, thrownError){
			$('#displayError').show();
			$('#displayError').append('server response: ' + xhr.status + '\n\r');
			$('#displayError').append('error description: ' + thrownError);
			// $('#displayError').append('<span style="color: red;">   Файл не создан!</span>');
			console.log('response: ' + xhr.status); // пoкaжeм oтвeт сeрвeрa
			console.log('error description: ' + thrownError); // и тeкст oшибки
			$('#GenerationExelDocument').prop('disabled', false);
			$('#RenameDefaultImage').prop('disabled', false);
		}
	});
});
</script>
<?php echo $footer; ?>